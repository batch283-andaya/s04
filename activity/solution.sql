SELECT * FROM artists WHERE name LIKE '%d%';

SELECT * FROM songs WHERE length > "00:03:50";

SELECT albums.name AS album_name, songs.name AS song_name, songs.duration AS song_length
FROM albums
JOIN songs ON albums.id = songs.album_id;

SELECT albums.name
FROM artists
JOIN albums ON artists.id = albums.artist_id
WHERE albums.name LIKE '%a%';

SELECT album_title FROM albums ORDER BY name DESC LIMIT 4;

SELECT albums.name AS album_name, songs.name AS song_name, songs.duration AS song_length
FROM albums
JOIN songs ON albums.id = songs.album_id
ORDER BY albums.name DESC;
